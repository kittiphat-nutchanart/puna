# puna
    object detection app for anyone and everyone 

# you can provide an image by
- upload your own image
- use your webcam to capture an image

# run the app locally

```
$ git clone https://gitlab.com/kittiphat-nutchanart/puna

$ cd puna

$ npm ci

# serve with hot reload at localhost:3000
$ npm run dev
```
now visit http://localhost:3000 to use the app

# the code
the most important piece of code is in puna/pages/index.vue